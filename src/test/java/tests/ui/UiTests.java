package tests.ui;

import com.codeborne.selenide.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class UiTests {
    @BeforeAll
    public static void setUp(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        Configuration.browserSize = "1920x1080";
        Configuration.browserCapabilities = new ChromeOptions().addArguments("--remote-allow-origins=*");
    }
    @Test
    public void launchBtnOnMainPageNavigateToAppTest(){
//открываем главную страницу
        Selenide.open("https://www.interswap.io/");
//кликаем на кнопку LAUNCH APP
        $(Selectors.byText("LAUNCH APP")).click();
//переключаемся на новую вкладку
        Selenide.switchTo().window(1);
//кликаем по первой сети в всплывающем окне
        $(Selectors.byText("Arbitrum Testnet")).click();
//проверяем что на новой странице отобразился обменник
        $(Selectors.byAttribute("data-onboarding", "swap")).should(Condition.visible);
    }
    @Test
    public void switchThemeDarkToLightTest(){
        Selenide.open("https://app.interswap.io/");
        $(Selectors.byText("Arbitrum Testnet")).click();
        $(Selectors.byAttribute("data-onboarding", "swap")).should(Condition.visible);
        $(Selectors.byAttribute("data-onboarding", "settings")).click();
        $(Selectors.byText("Light")).click();

    }
}
